'use strict';

// app.js
var express = require('express'),
    app = express(),
    fs = require('fs'),
    path = require('path'),
    server = require('http').createServer(app),
    io = require('socket.io')(server),
    cons = require('consolidate'),
    session = require('express-session'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser');

// Backend
//var admin = express();

// Config app
var appConfig = require('./config/app');
var socket = require('./middleware/socket');

// Include all module
app.use(express.static(__dirname + '/node_modules'));

// set session
app.use(cookieParser());
app.use(session({
    secret: "secret code",
    resave: false,
    saveUninitialized: true,
}));

//-- Set MVC --
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.engine('html', cons.swig);
app.set('views', path.join(__dirname, 'views'));

// app.set('view engine', 'jade');
app.set('view engine', 'html'); //
app.use(express.static(path.join(__dirname, 'public')));

// dynamically include routes (Controller)
var suffixeController = '',
    controllers = {}, ext = '.js';

fs.readdirSync('./controllers').forEach(function (file) {
    if (file.substr(-3) == '.js') {
        var name = file.substr(0, file.length - 3) + suffixeController;
        controllers[name] = require(__dirname + '/controllers/' + name);
        controllers[name].controller(app, express);

        var routeName = file.substr(0, file.length - 3 - suffixeController.length);
        if (fs.existsSync(__dirname + '/routes/' + routeName + ext)) {
            var route = require(__dirname + '/routes/' + routeName);
            app.use('/' + name, route);
        }
    }
});

// List all route
// app._router.stack.forEach(function (r) {
//     if (r.route && r.route.path) {
//         console.log(r.route.path)
//     }
// });

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

//-- End MVC --
server.listen(appConfig.port);

// Middleware
socket.init(io).call();