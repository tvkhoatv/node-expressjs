'use strict';

var express = require('express'),
    app = express(),
    router = express.Router();
// var path = process.cwd();

router.get('/index', function (req, res, next) {
    console.log(__dirname);
    res.render('user/index');
});

module.exports = router;