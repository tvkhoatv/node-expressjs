/**
 * Socket model
 *
 * @author Truong Vinh  Khoa
 */

module.exports = Socket = {  // an object
    io: null,       // by socket
    id: null,       // by key session id socket
    init: function (io) {
        try {
            this.io = io;
            this.id = io.id;
            return this;
        } catch (e) { console.log(e); }
    },
    get: function () {
        return this.io;
    },
    call: function () {
        this.io.on('connection', function (client) {
            console.log('Client connected...');

            client.on('join', function (data) {
                console.log(data);
                client.emit('messages', 'Hello from server');
            });

            client.on('messages', function (data) {
                console.log(data);
                // client.emit('broad', data);
                client.broadcast.emit('broad', data);
            });
        });
    }
}
